/*
O objetivo é fazer um juiz de Jokenpo que dada a jogada dos dois jogadores informa o resultado da partida.

As regras são as seguintes:

Pedra empata com Pedra e ganha de Tesoura
Tesoura empata com Tesoura e ganha de Papel
Papel empata com Papel e ganha de Pedra
*/

//link de ajuda com jasmine -> https://imasters.com.br/front-end/jasmine-entendendo-os-matchers

//Jokenpo

// Jogadas possíveis
// enum {
//   stone = 'PEDRA';
//   scissors = 'Tesoura';
//   paper = 'papel';
// }
const stone = 'PEDRA';
const scissors = 'Tesoura';
const paper = 'papel';

const AGULA = null;
const LAPIS = null;

const ERROR = 'ERROR';

// Resultados possíveis
const EMPATE = 'EMPATE';

function jokenpo(player1: string, player2: string): string {
  // testes para jogadas inválidas
  if (player1 === null ||  player2 === null ) {
    return ERROR;
  }


  //teste para caso de empate
  if (player1 ===  player2) {
    return EMPATE;
  }
  //teste para caso batalha de pedra x tesoura e teste de batalha tesoura x pedra
  if ((player1 === stone && player2 === scissors) || (player1 === scissors && player2 === stone)) {
    return stone;
  }

  // teste de batalha pedra x papel e teste de batalha papel x pedra
  if((player1 === stone && player2 === paper)|| (player1 === paper && player2 === stone)) {
    return paper;
  }

  // teste de batalha papel x tesoura

  if( (player1 === paper && player2 === scissors) || ( player2 === paper && player1 === scissors) ) {
    return scissors;
  }

  return ERROR;

}

// Atribuições

describe('CLINICARX: Teste de jokenpo123', () => {
  //-----Teste de empate
  it('Teste PEDRA PEDRA', () => expect(jokenpo(stone, stone)).toEqual(EMPATE));
  //-----
  it('Teste TESOURA TESOURA', () =>
    expect(jokenpo(scissors, scissors)).toEqual(EMPATE));
  //-----
  it('Teste PAPEL PAPEL', () => expect(jokenpo(paper, paper)).toEqual(EMPATE));
  //-----
  //-----Teste de batalha
  it("Teste PEDRA TESOURA", () => expect(jokenpo(stone, scissors)).toEqual(stone));
  //-----
  it("Teste TESOURA PEDRA", () => expect(jokenpo(scissors, stone)).toEqual(stone));
  //-----
  it("Teste PEDRA PAPEL", () => expect(jokenpo(stone, paper)).toEqual(paper));
  //-----
  it("Teste PAPEL PEDRA", () => expect(jokenpo(paper, stone)).toEqual(paper));
  //-----
  it("Teste PAPEL TESOURA", () => expect(jokenpo(paper, scissors)).toEqual(scissors));
  //-----
  it("Teste TESOURA PAPEL", () => expect(jokenpo(scissors, paper)).toEqual(scissors));
  //-----
  it("Teste Jojadores desconhecidos", () => expect(jokenpo(AGULA, LAPIS)).toEqual("ERROR"));
  //-----
});